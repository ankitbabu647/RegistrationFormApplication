package com.example.registrationformapplication.activity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.ActivityRecyclerViewFormateBinding

class RecyclerViewFormateActivity : AppCompatActivity() {
    private lateinit var binding:ActivityRecyclerViewFormateBinding

    @SuppressLint("RestrictedApi")
    public override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)

        binding=DataBindingUtil.setContentView(this,R.layout.activity_recycler_view_formate)

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)

    }

}